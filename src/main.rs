#[derive(Debug, Clone, Copy)]
enum Person {
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
}

#[derive(Debug, Clone, Copy)]
enum Colour {
    Black,
    White,
}

#[derive(Debug, Clone, Copy)]
struct Piece {
    person: Person,
    colour: Colour,
}

impl Piece {
    fn from(p: Person, c: Colour) -> Self {
        Self {
            person: p,
            colour: c,
        }
    }
}

#[derive(Debug)]
struct Board {
    board: [Option<Piece>; 64],
}

impl Board {
    fn new() -> Self {
        use crate::Colour::*;
        use crate::Person::*;

        let mut new = Self { board: [None; 64] };

        new.set(0, 0, Some(Piece::from(Rook, Black)));
        new.set(0, 1, Some(Piece::from(Knight, Black)));
        new.set(0, 2, Some(Piece::from(Bishop, Black)));
        new.set(0, 3, Some(Piece::from(Queen, Black)));
        new.set(0, 4, Some(Piece::from(King, Black)));
        new.set(0, 5, Some(Piece::from(Bishop, Black)));
        new.set(0, 6, Some(Piece::from(Knight, Black)));
        new.set(0, 7, Some(Piece::from(Rook, Black)));

        for f in 0..7 {
            new.set(1, f, Some(Piece::from(Pawn, Black)));
        }

        for f in 0..7 {
            new.set(6, f, Some(Piece::from(Pawn, White)));
        }

        new.set(7, 0, Some(Piece::from(Rook, White)));
        new.set(7, 1, Some(Piece::from(Knight, White)));
        new.set(7, 2, Some(Piece::from(Bishop, White)));
        new.set(7, 3, Some(Piece::from(Queen, White)));
        new.set(7, 4, Some(Piece::from(King, White)));
        new.set(7, 5, Some(Piece::from(Bishop, White)));
        new.set(7, 6, Some(Piece::from(Knight, White)));
        new.set(7, 7, Some(Piece::from(Rook, White)));

        new
    }

    fn set(&mut self, r: usize, f: usize, v: Option<Piece>) {
        self.board[8 * r + f] = v;
    }

    fn get(&self, r: usize, f: usize) -> Option<Piece> {
        self.board[8 * r + f]
    }
}

fn main() {
    let board = Board::new();
    println!("{:?}", board);
    println!("{:?}", board.get(1, 5).unwrap());
}
